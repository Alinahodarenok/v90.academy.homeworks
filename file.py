# 1. Имеется текстовый файл prices.txt с информацией о заказе из
# интернет магазина. В нем каждая строка с помощью символа
# табуляции \t разделена на три колонки:
# ● наименование товара;
# ● количество товара (целое число);
# ● цена (в рублях) товара за 1 шт.(целое число).


with open('prices.txt') as f:
    print(sum(eval('*'.join(s.split()[1:])) for s in f))


# 3. Имеется файл file.txt с текстом на латинице. Напишите программу,
# которая выводит следующую статистику по тексту:
# ● количество букв латинского алфавита;
# ● число слов;
# ● число строк.


with open('file.txt') as f:
    txt = f.read()
    print('Input file contains:')
    print(sum(map(str.isalpha, txt)), 'letters')
    print(len(txt.split()), 'words')
    print(txt.count('\n') + 1, 'lines')


# 2. Напишите программу, которая принимает поисковый запрос и
# выводит названия текстовых файлов, содержащих искомую
# подстроку. Все файлы располагаются в заданной директории.


import os

if __name__ == '__main__':

    folder = 'D:\\Python\\Textfiles'
    answ = set()
    search = input()
    for filename in os.listdir(folder):
        filepath = os.path.join(folder, filename)
        with open(filepath, 'r', encoding = 'utf-8') as fp:
            for line in fp:
                if search in line:
                    answ.add(filename)
                    for i in answ:
                        print(i)


# 5. С клавиатуры в одной строке вводится произвольное количество
# вещественных чисел. Запишите их в файл, расположив каждое число
# на отдельной строке.


a = [float(x) for x in input().split()]
with open('output.txt', 'w') as out:
    print(file=out, sep='\n')
